#!/opt/local/bin/perl



# ------------------------------------------------------------------------------------
# filename: noridWatcher.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://klevstul.com)
# started:  09.08.2009
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
# version:  v01_20090816
#           - First version of the program ready
# version:  v01_20090809
#           - Programming started
# ------------------------------------------------------------------------------------





# -------------------
# use
# -------------------
use LWP::Simple;
use LWP::UserAgent;
use HTTP::Request::Common qw(POST);
use strict;
use File::Copy;





# -------------------
# declarations / initializations
# -------------------
my $debug = 0;																						# prints out debugging info if variable equals 1

my $userAgent = LWP::UserAgent->new;

my @commentsFromFile;																				# comments read from file (comments will always be moved to the top)
my @domainsAll;																						# contains ALL domains, new and from db, valid and invalid

# PRE-PROCESSED domains read from database file
my @dbDomainsAvailableNew;																			# available (new domains)
my @dbDomainsAvailableReleased;																		# available (released)
my @dbDomainsReserved;																				# reserved
my @dbDomainsRegistered;																			# registered
my @dbDomainsUnknown;																				# unknown

# NEWLY read domains from DB to be checked
my @domainsToCheck;

# result AFTER processing, domains only
my @domainsAvailableNew;
my @domainsAvailableReleased;

# result AFTER processing with a valid status (status + domain)
my @processedDomainsInvalid;																		# processed and new domains on wrong format
my @processedDomainsUnknown;																		# domains we don't know the status for due to errors while processing
my @processedDomainsChecked;																		# domains that has been checked
my @processedDomainsNotChecked;																		# domains that has not been checked

# valid statuses and tags
my $statusAvailableNew		= 'AVAILABLE_NEW';
my $statusAvailableReleased	= 'AVAILABLE_RELEASED';
my $statusInvalid			= 'INVALID';
my $statusRegistered		= 'REGISTERED';
my $statusReserved			= 'RESERVED';
my $statusUnknown			= 'UNKNOWN';
my $splitTag				= ' :: ';

# directories to use
my $backupDir				= 'backup';
my $outputDir				= 'output';
my $inputDir				= 'input';
my $logDir					= 'log';

# main database file
my $domainsDatabaseFile		= $inputDir . '/noridWatcher.db';

# import file
my $importFile				= $inputDir . '/import.txt';

# tmp database file
my $tmpDatabaseFile			= $outputDir . '/noridWatcher_' . timestamp() . '.db';


# - - - - -
# What to check
#
# Values: 	0, 1
# Note:		$checkNew			: check new domains imported
#			$checkRegistered	: re-check domains that has been stored as registered earlier
#			$checkReserved		: re-check domains that has been stored as reserved earlier
#			$checkAvailable		: re-check domains that has been available earlier
#			$checkUnknown		: re-check domains with unknown status
# - - - - -
my $checkNew		= 1;
my $checkRegistered	= 0;
my $checkReserved	= 0;
my $checkAvailable	= 0;
my $checkUnknown	= 1;


# - - - - -
# Seconds to wait
#
# Values: 	number (seconds)
# Note:		$waitMin			: The minimum number of seconds to wait
#			$waitMax			: The maximum number of seconds to wait
#			This program will pause a random number of seconds in between min and max in between each time
#			it check the status for a domain. This is done to fake normal user behaviour.
# - - - - -
my $waitMin = 5;
my $waitMax = 15;


# - - - - -
# Request method
#
# Values: 	'POST', 'GET'
# Note:		For POST the variables are hardcoded in the checkDomain() routine. Might have to be updated.
#			NB: POST method doesn't seem to work for the Norid site.
# - - - - -
my $requestMethod = 'GET';


# - - - - -
# HTTP referer
#
# Note:		This will be the refering page to the target URL, should often be same as "$preURL"
#			if you want this to make sense.
# - - - - -
my $httpReferer = 'http://norid.no';
#my $httpReferer = 'http://klevstul.com';


# - - - - -
# Pre URL
#
# Note:		This URL will be accessed first. Usefull when for example voting, so it looks like
#			a user loads a page before he actually votes. This should be the same as the HTTP
#			referer page to make sense. If 'Pre URL' is set, this must return success when trying
#			to load the object. If not the 'Target URL' won't be loaded.
# - - - - -
my $preUrl = 'http://norid.no';
#my $preUrl = 'http://klevstul.com';


# - - - - -
# URL to request
#
# Note: 	If you're using POST request method the parameters to send has to be set in the
#			checkDomain() sub-routine. If you're using GET you have to set them in the url.
#			The string '[domain]' will be replaced with the domain to check
# Example:	http://www.norid.no/domenenavnbaser/whois/index.php3?charset=UTF-8&query=[domain]&sok=s%C3%B8k 	(GET method URL)
#			http://www.norid.no/domenenavnbaser/whois/index.php3											(POST method URL)
# - - - - -
my $targetUrl = 'http://www.norid.no/domenenavnbaser/whois/index.php3?charset=UTF-8&query=[domain]&sok=s%C3%B8k';		# GET 
#my $targetUrl = 'http://www.norid.no/domenenavnbaser/whois/index.php3';												# POST
#my $targetUrl = 'http://watashi.no/kCom.i/cgi/kFeeder.cgi';
#my $targetUrl = 'http://klevstul.com';


# - - - - -
# Logfiles
# - - - - -
my $fileLog = 'log_noridWatcher.log';


# - - - - -
# Lockfile
# - - - - -
my $lockFile = 'lockfile_noridWatcher.lock';


# - - - - -
# Initialize user agent
# - - - - -
$userAgent->timeout(60);																			# timeout, in seconds
$userAgent->agent('Mozilla/5.0');																	# identify user agent as Mozilla





# -------------------
# main
# -------------------


# lock, to prevent multiple instances running at the same time
debug("lockIt()");
lockIt();


# print start
debug("printStart()");
printStart();


# backup database file
debug("backupFile()");
backupFile($domainsDatabaseFile);


# load domains from database file
debug("loadDomainFile()");
loadDomainFile($domainsDatabaseFile);


# import new domains from import file
debug("importFromFile()");
importFromFile($importFile);


# check loaded domains
debug("checkDomains()");
checkDomains();


# store data in database file
debug("saveToDb()");
saveToDb();


# save interesting results (if any) to ouput files
debug("saveToOutputFiles()");
saveToOutputFiles();


# print summary
debug("printStop()");
printStop();


# open the lock / delete the lockfile
debug("unlockIt()");
unlockIt();





# -------------------
# sub
# -------------------
sub importFromFile{
	my $file = $_[0];
	my $filesize = -s $file;

	if ($filesize > 0){
		debug("import file '$file'");

		# backup file
		backupFile($file);
	
		# load domains from file
		loadDomainFile($file);
	
		# empty file
		open (IMPORTFILE, ">$file") || error("Could not open '$file'", 1);
		print IMPORTFILE "";
		close (IMPORTFILE);
	}
}


sub loadDomainFile{
	my $file = $_[0];
	my $line;
	my $status;
	my $domain;

	debug("load domain file '$file'");

	open (DOMAINFILE, "<$file") || error("Could not open '$file'", 1);

	WHILE: while ($line = <DOMAINFILE>){
		$line =~ s/\n//;

		debug("process '$line'");

		# commented line
		if ($line =~ m/^\s?#/){
			push(@commentsFromFile, $line);

		# ----------------------------------
		# processed line
		# ----------------------------------
		} elsif ($line =~ m/^$splitTag/){

			# correct format of a processed line
			if ($line =~ m/^$splitTag(\w+)$splitTag(.*)/){
				$status = $1;
				$domain = $2;

				# duplicate check of pre-processed domains
				if (inArray($domain, @domainsAll)){
					error("Duplicate: " . $domain);
					next WHILE;
				} else {
					push(@domainsAll, $domain);
				}

				# invalid domains
				if ($status eq $statusInvalid){
					push(@processedDomainsInvalid, $line);

				# valid domains
				} else {
					if ($status eq $statusAvailableNew){
						push(@dbDomainsAvailableNew, $domain);
					} elsif ($status eq $statusAvailableReleased){
						push(@dbDomainsAvailableReleased, $domain);
					} elsif ($status eq $statusRegistered){
						push(@dbDomainsRegistered, $domain);
					} elsif ($status eq $statusReserved){
						push(@dbDomainsReserved, $domain);
					} elsif ($status eq $statusUnknown){
						push(@dbDomainsUnknown, $domain);
					}
				}

			# invalid formatted line
			} else {
				error("invalid format of line:\n" . $line, 1);
			}


		# ----------------------------------
		# new line (not processed before)
		# ----------------------------------
		} else {

			if ($line =~ m/^((\S)+(\.no))\s?$/){
				$domain = $1;
				$domain =~ s/\n//;

				# duplicate check of new domains
				if (inArray($domain, @domainsAll)){
					error("Duplicate: " . $domain);
					next WHILE;
				} else {
					push(@domainsAll, $domain);
					push(@domainsToCheck, $line);
				}

			} else {
				$line =~ s/\n//;
				push(@processedDomainsInvalid, $splitTag . $statusInvalid . $splitTag . $line);
			}

		}

	}
	close (DOMAINFILE);
}


sub saveToDb{
	arrayToFile(">$domainsDatabaseFile", @commentsFromFile);
	arrayToFile(">>$domainsDatabaseFile", sort(@processedDomainsChecked));
	arrayToFile(">>$domainsDatabaseFile", sort(@processedDomainsUnknown));
	arrayToFile(">>$domainsDatabaseFile", sort(@processedDomainsInvalid));
	arrayToFile(">>$domainsDatabaseFile", sort(@processedDomainsNotChecked));
}


sub saveToOutputFiles{
	# save list of released domains
	if ($#domainsAvailableReleased >= 0){
		arrayToFile(">>".$outputDir."/" . timestamp() . "_" . $statusAvailableReleased . ".txt", @domainsAvailableReleased);
	}
	
	# save list of available new domains
	if ($#domainsAvailableNew >= 0){
		arrayToFile(">>".$outputDir."/" . timestamp() . "_" . $statusAvailableNew . ".txt", @domainsAvailableNew);
	}
}


sub checkDomains{
	# check new domains
	if ($checkNew != undef) {
		checkDomainsArray(@domainsToCheck);
	} else {
		addToNotChecked($statusUnknown, @domainsToCheck);
	}

	# check reserved domains
	if ($checkReserved != undef) {
		checkDomainsArray(@dbDomainsReserved);
	} else {
		addToNotChecked($statusReserved, @dbDomainsReserved);
	}

	# check registered domains
	if ($checkRegistered != undef){
		checkDomainsArray(@dbDomainsRegistered);
	} else {
		addToNotChecked($statusRegistered, @dbDomainsRegistered);
	}

	# check available domains
	if ($checkAvailable != undef){
		checkDomainsArray(@dbDomainsAvailableNew);
		checkDomainsArray(@dbDomainsAvailableReleased);
	} else {
		addToNotChecked($statusAvailableNew, @dbDomainsAvailableNew);
		addToNotChecked($statusAvailableReleased, @dbDomainsAvailableReleased);
	}

	# check unknown domains
	if ($checkUnknown != undef){
		checkDomainsArray(@dbDomainsUnknown);
	} else {
		addToNotChecked($statusUnknown, @dbDomainsUnknown);
	}
}


sub addToNotChecked{
	my ($status, @array) = @_;

	foreach (@array){
		push(@processedDomainsNotChecked, $splitTag . $status . $splitTag . $_);
	}
}


sub checkDomainsArray{
	my @array = @_;

	foreach (@array){
		checkDomain($_);
		sleep( randomNumber($waitMin, $waitMax) );
	}
}


sub checkDomain{
	my $domain = $_[0];
	my $response;
	my $request;
	my $returnObject;
	my $_targetUrl;

	$_targetUrl = $targetUrl;
	$_targetUrl =~ s/\[domain\]/$domain/sgi;

	if ($requestMethod eq "GET"){																	# GET request method (sends all parameters in URL)
		$request = HTTP::Request->new(GET => $_targetUrl);
	} else {
		$request = POST $_targetUrl, [charset => 'UTF-8', query => $domain, sok => 's%C3%B8k'];		# POST request method
		# $request = POST $_targetUrl, [p_feeder => 'kFeeder_kcomiBlogspot_latest', p_action => 'show'];
	}

	$request->referer($httpReferer);																# set HTTP referer

	if ($preUrl ne ""){
		$response = $userAgent->get($preUrl);														# request preUrl using GET method (direct with no referer)
	}

	if ( ($preUrl ne "" && $response->is_success) || $preUrl eq ""){
		$response = $userAgent->request($request);													# request object (with referer)
	}

	if ($response->is_success){
		$returnObject = $response->content;                                          
		interpretResult($domain, $returnObject);
	} else {
		&error("\n" . $response->content);
	}
}


sub interpretResult{
	my $domain = $_[0];
	my $result = $_[1];
	my $domainStatus;
	my $logMessage;
	my $statusLine;

	if ($result =~ m/kopirett.en.html<\/A>\n\n(.*)/){
		$domainStatus = $1;

		# reserved domain that can not be registered (yet)
		if ($domainStatus =~ m/This domain can currently not be registered/i){
			$logMessage = "$domain > $statusReserved";
			$statusLine = $splitTag . $statusReserved . $splitTag . $domain;
			push(@processedDomainsChecked, $splitTag . $statusReserved . $splitTag . $domain);

		# available domain
		} elsif ($domainStatus =~ m/no matches/i){
			# domain was reserved earlier
			if (inArray($domain, @dbDomainsReserved)){
				$logMessage = "$domain > $statusAvailableReleased";
				$statusLine = $splitTag . $statusAvailableReleased . $splitTag . $domain;
				push(@processedDomainsChecked, $splitTag . $statusAvailableReleased . $splitTag . $domain);
				push(@domainsAvailableReleased, $domain);

			# domain is new, and has not been reserved earlier (as far as we know)
			} else {
				$logMessage = "$domain > $statusAvailableNew";
				$statusLine = $splitTag . $statusAvailableNew . $splitTag . $domain;
				push(@processedDomainsChecked, $splitTag . $statusAvailableNew . $splitTag . $domain);
				push(@domainsAvailableNew, $domain);
			}

		# registered domain
		} elsif ($domainStatus =~ m/Domain information/i){
			$logMessage = "$domain > $statusRegistered";
			$statusLine = $splitTag . $statusRegistered . $splitTag . $domain;
			push(@processedDomainsChecked, $splitTag . $statusRegistered . $splitTag . $domain);

		# unknown response
		} else {
			$statusLine = $splitTag . $statusUnknown . $splitTag . $domain;
			push(@processedDomainsUnknown, $splitTag . $statusUnknown . $splitTag . $domain);
			error("Unknown response line for domain " . $domain . ": '" . $domainStatus . "'");
		}

	} else {
		$statusLine = $splitTag . $statusUnknown . $splitTag . $domain;
		push(@processedDomainsUnknown, $splitTag . $statusUnknown . $splitTag . $domain);
		error("Unknown result from Norid for domain " . $domain . ": '" . $domainStatus . "'");
	}

	# print to log and screen
	writeLog($logMessage);
	print $logMessage . "\n";

	# write to temporary database file (this file might come handy if program crash / or you have to stop it)
	writeTmpDatabaseFile($statusLine);

}


sub printStart{
	print "-----------------------------\n";
	print "------ noridWatcher.pl ------\n";
	print "-----------------------------\n";
	print "\n";
	print "HTTP referer  : $httpReferer\n";
	print "Pre URL       : $preUrl\n";
	print "Target URL    : $targetUrl\n";
	print "Request method: $requestMethod\n";
	print "- - - - - - - - - - - - - - -\n";

	writeLog(
		 "=================================================\n"
		."Starting...\n"
		."HTTP referer:   $httpReferer\n"
		."Pre URL:        $preUrl\n"
		."Target URL:     $targetUrl\n"
		."Request method: $requestMethod"
	);
}


sub printStop{
	my $msg;

	$msg .= "- - - - - - - - - - - - - - -\n";
	$msg .= "*** DB STATUS ***\n";
	$msg .= "# db total              : " . ($#domainsAll+1) . "\n";
	$msg .= "# db valid                : " . (($#domainsAll+1)-($#processedDomainsInvalid+1)) . "\n";
	$msg .= "# db avail. new             : " . ($#dbDomainsAvailableNew+1) . "\n";
	$msg .= "# db avail. released        : " . ($#dbDomainsAvailableReleased+1) . "\n";
	$msg .= "# db reserved               : " . ($#dbDomainsReserved+1) . "\n";
	$msg .= "# db registered             : " . ($#dbDomainsRegistered+1) . "\n";
	$msg .= "# db unknown                : " . ($#dbDomainsUnknown+1) . "\n";
	$msg .= "# db (+new) invalid       : " . ($#processedDomainsInvalid+1) . "\n";

	$msg .= "*** PROCESSED STATUS ***\n";
	$msg .= "# valid processed       : " . ($#processedDomainsChecked+1) . "\n";
	$msg .= "# unknown processed     : " . ($#processedDomainsUnknown+1) . "\n";
	$msg .= "# not processed         : " . ($#processedDomainsNotChecked+1) . "\n";

	$msg .= "*** PROCESSED RESULTS ***\n";
	$msg .= "# found avail. new      : " . ($#domainsAvailableNew+1) . "\n";
	$msg .= "# found avail. released : " . ($#domainsAvailableReleased+1) . "\n";
	$msg .= "-----------------------------\n";

	print $msg;

	writeLog(
		 "\n"
		.$msg
		."Stopping...     : ================================================="
	);
}


sub writeLog{
	my $line = $_[0];
	my $timestamp = &timestamp;

	open (LOGFILE, ">>$logDir"."/".$fileLog) || error("Could not open '$fileLog'", 1);
	print LOGFILE $timestamp . " : " . $line ."\n";
	close (LOGFILE);
}


sub writeTmpDatabaseFile{
	my $line = $_[0];

	open (TMPDBFILE, ">>$tmpDatabaseFile") || error("Could not open '$tmpDatabaseFile'", 1);
	print TMPDBFILE $line ."\n";
	close (TMPDBFILE);
}


sub timestamp{
	my $time = time;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
	my $timestamp;

	$year = $year+1900;
	$mon = $mon+1;

	# puts a zero in front of one digit.
	$sec =~ s/^(\d){1}$/0$1/;
	$min =~ s/^(\d){1}$/0$1/;
	$hour =~ s/^(\d){1}$/0$1/;
	$mday =~ s/^(\d){1}$/0$1/;
	$mon =~ s/^(\d){1}$/0$1/;

	# 19991109_182455
	$timestamp = $year . $mon . $mday . "_" . $hour . $min . $sec;

	return $timestamp;
}


sub lockIt{
	my $msg = "Lockfile exist. Please stop program and/or delete lockfile to start new instance.\n";

	if (-e $lockFile){
		print $msg;
		&writeLog(
			 "\n"
			.$msg
			."                : ================================================="
		);
		exit;
	} else {
		open (LOCKFILE, ">$lockFile") || print("ERROR: Could not open '$lockFile'");
		flock (LOCKFILE, 2);
		print LOCKFILE "";
		close (LOCKFILE);
		flock (LOCKFILE, 8);
	}
}


sub unlockIt{
	if (-e $lockFile){
		unlink $lockFile;
	}
}


sub error{
	my $msg		= $_[0];
	my $exit	= $_[1];

	$msg = "ERROR: " . $msg . "\n";

	print $msg;
	&writeLog($msg);

	if ($exit != undef){
		exit 0;
	}
}


sub debug{
	my $msg		= $_[0];

	if ($debug != undef){
		$msg = "DEBUG: " . $msg . "\n";
		print $msg;
	}
}


sub arrayToFile{
	my ($file, @array) = @_;																		# note: the first argument "$file" should include ">" if overwrite or ">>" if append

	open (ARRAYTOFILE, $file) || error("ERROR: Could not open '$file'", 1);
	foreach ( @array ){
		print ARRAYTOFILE $_ ."\n";
	}
	close (ARRAYTOFILE);
}


sub inArray{
	my ($entry, @array) = @_;

	foreach (sort @array){
		if ($entry eq $_){
			return 1;																				# we do have an entry
		}
	}
	return 0;																						# didn't find anything
}


sub printArray{
	my @array = @_;

	foreach (sort @array){
		print $_ . "\n";
	}
}


sub backupFile {
	my $file = $_[0];

	$file =~ /(?:.*\/)?(.+)/s;
	my $filename = $1; 

	copy($file, $backupDir . "/" . timestamp() .  "_" . $filename) or error("Unable to backup the file " . $file, 1);
}


sub randomNumber{
	my $low   = $_[0];
	my $heigh = $_[1];
	my $random;

	$heigh++;																						# add 1 to heigh so rand will return the right number
	# example:
	# low = 10, heigh = 21
	# heigh = 11
	$heigh = $heigh - $low;

	# random in between 0-11
	$random = rand($heigh);
	#random in between 10-21
	$random += $low;

	return int($random);																			# using int the number will be rounded, like floor()
}
