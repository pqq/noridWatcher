#!/opt/local/bin/perl



# ------------------------------------------------------------------------------------
# filename: dictionary2import.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://klevstul.com)
# started:  16.08.2009
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
# version:  v01_20090816
#           - Programming started
# ------------------------------------------------------------------------------------





# -------------------
# use
# -------------------
use strict;





# -------------------
# declarations / initializations
# -------------------
my $dictionaryFile	= 'input/wordlist1.txt';

my $outputFile		= 'input/import.txt';
my $domainEnding	= '.no';
my $minWordLength	= 4;
my $maxWordLength	= 5;
my @words;





# -------------------
# main
# -------------------
processFile($dictionaryFile, $outputFile);





# -------------------
# sub
# -------------------
sub processFile{
	my $inputFile	= $_[0];
	my $outputFile	= $_[1];
	my $line;

	# read from file
	open (FILE, "<$inputFile") || die("ERROR: Could not open '$inputFile'");
	WHILE: while ($line = <FILE>){
		$line =~ s/\n//;
		$line =~ s/\r//;

		# { = �
		# | = �
		# } = �
		if ($line =~ m/[\{|\}|\|]/){
			next WHILE;
		}

		if ( length($line) > $maxWordLength || length($line) < $minWordLength ){
			next WHILE;
		}

		push(@words, $line);
	}
	close (FILE);

	# write to file
	open (FILE, ">>$outputFile") || die("ERROR: Could not open '$outputFile'");
	foreach ( @words ){
		print FILE $_ . $domainEnding . "\n";
	}
	close (FILE);

	print "$#words domains added to $inputFile\n";
}

